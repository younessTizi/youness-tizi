package ma.ac.emi.qcm.controller;

import ma.ac.emi.qcm.entities.Eleve;
import ma.ac.emi.qcm.repository.EleveRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/eleves")
public class EleveController {
	@Autowired
	EleveRepository er;

	@GetMapping
	public String index(Model model) {
		model.addAttribute("eleves", er.findAll());
		model.addAttribute("newEleve", new Eleve());
		return "eleves/Eleves";
	}

	@PostMapping("/addEleve")
	public String addEleve(@ModelAttribute("SpringWeb") Eleve newEleve, Model model) {
		er.save(newEleve);
		model.addAttribute("eleves", er.findAll());
		return "redirect:/eleves";
	}
}
